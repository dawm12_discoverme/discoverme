package com.discoverme.www.repository;

import com.discoverme.www.domain.Experiencia;
import com.discoverme.www.domain.Usuario;
import java.util.List;

/**
 * Interficie que representa un repositorio de experiencia
 *
 * @author Manuel Leyva
 */
public interface ExperienciaRepository {
    List<Experiencia>getAllExperiencias();
    List<Experiencia> getAllExperiencias(String orden);
    Experiencia getExperienciaById(Integer id);
    Experiencia getExperienciaByNombre(String nombre);
    List<Experiencia> getExperienciaByUsuario(Usuario usuario);
    void addExperiencia(Experiencia experiencia);
    void deleteExperiencia(Experiencia experiencia);
}
