package com.discoverme.www.repository;

import com.discoverme.www.domain.Perfil;
import com.discoverme.www.domain.Rol;
import com.discoverme.www.domain.Usuario;
import java.util.List;

/**
 * Interficie que representa un repositorio de usuarios
 *
 * @author Manuel Leyva
 */
public interface UsuarioRepository {
    List<Usuario> getUsuariosByRol(Rol rol);
    List<Usuario> getUsuariosByPerfil(Perfil rol);
    Usuario getUsuarioById(String id);
    void addUsuario(Usuario usuario);
    void deleteUsuario(Usuario usuario);
}
