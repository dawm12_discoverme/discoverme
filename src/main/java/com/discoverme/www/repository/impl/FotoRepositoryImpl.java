package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Foto;
import com.discoverme.www.repository.FotoRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Clase que representa un repositorio de fotos y con ella accedemos a la BD y recuperamso los datos que se necesiten
 * @author leyva
 */
@Transactional
@Repository("fotoRepositoryImpl")
public class FotoRepositoryImpl implements FotoRepository{
    
    @Autowired
    private SessionFactory sessionFactory;

    /**
    * Funcion que obtiene foto por id
    * @param id
    * @return Foto de la BD
    * @author Manuel Leyva    
    */
    @Override
    public Foto getFotoById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));        
        return (Foto) criteria.uniqueResult();
    }
    
    /**
    * Funcion que obtiene foto por nombre
    * @param nombre
    * @return Foto de la BD
    * @author Manuel Leyva    
    */
    @Override
    public Foto getFotoByNombre(String nombre) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("nombre", nombre));        
        return (Foto) criteria.uniqueResult();
    }

    /**
    * Funcion que agrega una foto
    * @param foto
    * @author Manuel Leyva    
    */
    @Override
    public void addFoto(Foto foto) {
        getSession().saveOrUpdate(foto);
    }
    
    /**
    * Funcion que crea una session para conectarse a la BD
    * @return La sesion
    * @author Manuel Leyva    
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    
    /**
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Manuel Leyva    
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Foto.class);
    }
    
    /**
     * Funcion que elimina una foto
     * @param foto 
     * @author Manuel Leyva
     */
    @Override
    public void deleteFoto(Foto foto) {
        getSession().delete(foto);
    }
    
    /**
     * Funcion que agrega avatares 
     * @return 
     * @author Manuel Leyva
     */
    @Override
    public List<Foto> getAllAvatares() {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.like("foto","Avatar", MatchMode.START));        
        return (List<Foto>) criteria.list();
    }

}
