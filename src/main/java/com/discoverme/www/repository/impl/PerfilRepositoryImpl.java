package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Perfil;
import com.discoverme.www.repository.PerfilRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Clase que representa un repositorio de perfiles de huesped
 *
 * @author Manuel Leyva
 */
@Transactional
@Repository("perfilRepositoryImpl")
public class PerfilRepositoryImpl implements PerfilRepository {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    /**
    * Funcion que pasandole una id devuelve el perfil correspondiente
    * @param id Identificador del usuario que queremos recuperar
    * @return Usuario de la BD
    * @author Manuel Leyva
    */
    @Override
    public Perfil getPerfilById(Integer id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Perfil) criteria.uniqueResult();
    }

    /**
    * Funcion que pasandole una nombre devuelve el perfil correspondiente
    * @param nombre, es el nombre del usuario que queremos recuperar
    * @return Usuario de la BD
    * @author Manuel Leyva
    */    
    @Override
    public Perfil getPerfilByNombre(String nombre) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("nombre", nombre));
        return (Perfil) criteria.uniqueResult();
    }
    
    /**
    * Funcion que devuelve todos los perfiles de la BD
    * @return Listado completo de perfiles de la BD
    * @author Manuel Leyva    
    */
    @Override
    public List<Perfil> getAllPerfiles() {
        Criteria criteria = createEntityCriteria();
        return (List<Perfil>) criteria.list();
    }
    /**
    * Funcion que crea una session para conectarse a la BD
    * @return La sesion
    * @author Manuel Leyva    
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    
    /**
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Manuel Leyva    
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Perfil.class);
    }
}
