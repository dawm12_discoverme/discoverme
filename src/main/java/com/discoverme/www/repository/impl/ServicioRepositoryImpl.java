package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Servicio;
import com.discoverme.www.repository.ServicioRepository;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;
/**
 * Clase que representa el repositorio de servicios con la cual accedemos a la BD y recuperamos los datos que se necesiten
 * @author Jose Luis Sanchez Escoda
 */
@Transactional
@Repository ("servicioRepositoryImpl")
public class ServicioRepositoryImpl implements ServicioRepository {
    @Autowired
    private SessionFactory sessionFactory;
    
    /**
    * Funcion devuelve todos los servicios de la base de datos
    * @return Listado completo de los servicios de la base de datos
    * @author Jose Luis Sanchez Escoda
    */
    @Override
    public List<Servicio>getAllServicios() {
        Criteria criteria = createEntityCriteria();    
        return (List<Servicio>) criteria.list();   
    }
    
    /**
    * Funcion que pasandole un id devuelve el servicio correspondiente
    * @param id Identificador del servicio que queremos recuperar
    * @return Servicio de la base de datos
    * @author Jose Luis Sanchez Escoda
    */
    @Override
    public Servicio getServicioById(Integer id){
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id",id));
        return (Servicio) criteria.uniqueResult();
    }
    
    /**
     * Funcion que devuelve un servicio dado su nombre
     * @param nombre
     * @return 
     * @author Jose Luis Sanchez Escoda 
     */
    @Override
    public Servicio getServicioByNombre(String nombre) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("nombre",nombre));
        return (Servicio) criteria.uniqueResult();
    }
    /**
    * Funcion que guarda en la BD un servicio
    * @param servicio Servicio para guardar en la base de datos
    * @author Jose Luis Sanchez Escoda  
    */
    @Override
    public void addServicio(Servicio servicio){
        getSession().saveOrUpdate(servicio);
    }
    /**
    * Funcion que crea una session para conectarse a la base de datos
    * @return La sesion
    * @author Jose Luis Sanchez Escoda 
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    /**
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Jose Luis Sanchez Escoda 
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Servicio.class);
    }
    
    /**
     * Funcion que elimina servicios
     * @param servicio 
     * @author Jose Luis Sanchez Escoda 
     */
    @Override
    public void deleteServicio(Servicio servicio) {
        getSession().remove(servicio);
    }
    
}
