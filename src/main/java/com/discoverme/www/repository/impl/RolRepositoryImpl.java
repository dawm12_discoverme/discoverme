/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Rol;
import com.discoverme.www.repository.RolRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Clase que representa un repositorio de roles con ella accedemos a la BD y recuperamos los datos que se necesiten
 * @author Manuel Leyva
 */
@Transactional
@Repository("rolRepositoryImpl")
public class RolRepositoryImpl implements RolRepository{
    
    @Autowired
    private SessionFactory sessionFactory;

    /**
    * Funcion que devuelve todos los roles de la BD por su id
    * @param id
    * @return Listado de roles por id de la BD
    * @author Manuel Leyva    
    */    
    @Override
    public Rol GetRolById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Rol) criteria.uniqueResult();
    }

    /**
    * Funcion que devuelve todos los roles de la BD por su nombre
     * @param nombre
    * @return Listado de roles por nombre que hay en la BD
    * @author Manuel Leyva    
    */     
    @Override
    public Rol GetRolByNombre(String nombre) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("nombre", nombre));
        return (Rol) criteria.uniqueResult();
    }
    /**
    * Funcion que crea una session para conectarse a la BD
    * @return La sesion
    * @author Manuel Leyva    
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    
    /**
    * Funcion que devuelve todos los roles de la BD
    * @return Listado completo de roles de la BD
    * @author Manuel Leyva    
    */
    @Override
    public List<Rol> getAllRoles() {
        Criteria criteria = createEntityCriteria();
        return (List<Rol>) criteria.list();
    }
    /**
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Manuel Leyva    
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Rol.class);
    }
}
