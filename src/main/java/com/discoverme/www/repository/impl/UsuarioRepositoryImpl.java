package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Perfil;
import com.discoverme.www.domain.Rol;
import com.discoverme.www.domain.Usuario;
import com.discoverme.www.repository.UsuarioRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Clase que representa un repositorio de usuarios con ella accedemos a la BD y recuperamos los datos que se necesiten
 * @author Manuel Leyva
 */
@Transactional
@Repository("usuarioRepositoryImpl")
public class UsuarioRepositoryImpl implements UsuarioRepository{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    /**
    * Funcion que devuelve la lista de usuarios
    * @param rol
    * @return Usuario de la BD
    * @author Manuel Leyva
    */
    @Override
    public List<Usuario> getUsuariosByRol(Rol rol) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("rol", rol));
        return (List<Usuario>) criteria.list();
    }
    
    /**
     * Funcion que agrega perfiles
     * @param perfil
     * @return 
     * @author Manuel Leyva
     */
    @Override
    public List<Usuario> getUsuariosByPerfil(Perfil perfil) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("perfil", perfil));
        return (List<Usuario>) criteria.list();
    }
    
    /**
    * Funcion que pasandole una id devuelve el usuario correspondiente
    * @param id Identificador del usuario que queremos recuperar
    * @return Usuario de la BD
    * @author Manuel Leyva
    */
    @Override
    public Usuario getUsuarioById(String id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Usuario) criteria.uniqueResult();
    }
    
    /**
     * Funcion que agrega un usuario
     * @param usuario 
     * @author Manuel Leyva
     */
    @Override
    public void addUsuario(Usuario usuario) {
        getSession().saveOrUpdate(usuario);
    }
    
    /**
     * Funcion que elimina un usuario
     * @param usuario 
     * @author Manuel Leyva
     */
    @Override
    public void deleteUsuario(Usuario usuario) {
        getSession().remove(usuario);
    }
    
    /**
    * Funcion que crea una session para conectarse a la BD
    * @return La sesion
    * @author Manuel Leyva    
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    
    /**
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Manuel Leyva    
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Usuario.class);
    }

}
