package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Comentario;
import com.discoverme.www.domain.Experiencia;
import com.discoverme.www.repository.ComentarioRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Clase que representa un repositorio de comentarios con ella accedemos a la BD y recuperamso los datos que se necesiten
 * @author Manuel Leyva
 */
@Transactional
@Repository("comentarioRepositoryImpl")
public class ComentarioRepositoryImpl implements ComentarioRepository {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    /**
    * Funcion que devuelve todos los comentarios de la bd
    * @return Listado de comentarios
    * @author Manuel Leyva    
    */
    @Override
    public List<Comentario> getAllComentarios() {
        Criteria criteria = createEntityCriteria();
        return (List<Comentario>) criteria.list();
    }
    
    /**
    * Funcion que devuelve todos los comentarios de una experiencia
    * @param experiencia del cual queremos que devuelva los comentarios
    * @return Listado de comentarios
    * @author Manuel Leyva    
    */
    @Override
    public List<Comentario> getComentariosByIdExperiencia(Experiencia experiencia) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("experiencia", experiencia));
        return (List<Comentario>) criteria.list();
    }
    
    /**
     * Funcion que devuelve el comentario de una experiencia dado un id
     * @param id
     * @return 
     * @author Manuel Leyva
     */
    @Override
    public Comentario getComentarioById(Integer id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Comentario) criteria.uniqueResult();
    }
    
    /**
    * Funcion que guarda en la BD un comentario
    * @param comentario Comentario para guardar en la BD
    * @author Manuel Leyva    
    */
    @Override
    public void addComentario(Comentario comentario) {
        getSession().saveOrUpdate(comentario);
    }

    /**
    * Funcion que crea una session para conectarse a la BD
    * @return La sesion
    * @author Manuel Leyva    
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    
    /**
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Manuel Leyva    
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Comentario.class);
    }
    
    /**
     * Funcion que elimina un comentario
     * @param comentario 
     * @author Manuel Leyva
     */
    @Override
    public void deleteComentario(Comentario comentario) {
        getSession().delete(comentario);
    }

}
