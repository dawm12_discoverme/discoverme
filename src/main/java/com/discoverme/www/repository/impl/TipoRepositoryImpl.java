package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Tipo;
import com.discoverme.www.repository.TipoRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Clase que representa un repositorio de tipos/preferencias de experiencias con ella accedemos a la BD y recuperamso los datos que se necesiten
 * @author Manuel Leyva
 */
@Transactional
@Repository("tipoRepositoryImpl")
public class TipoRepositoryImpl implements TipoRepository{
    
    @Autowired
    private SessionFactory sessionFactory;

    /**
    * Funcion que pasandole una id devuelve el tipo/preferencia correspondiente
    * @param id Identificador del tipo/preferencia que queremos recuperar
    * @return Tipo/preferencia de la BD
    * @author Manuel Leyva
    */
    @Override
    public Tipo getTipoById(Integer id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Tipo) criteria.uniqueResult();
    }
    
    /**
    * Funcion que devuelve todos los tipos/preferencias de la BD
    * @return Listado completo de tipos/preferencias de la BD
    * @author Manuel Leyva    
    */
    @Override
    public List<Tipo> getAllTipos() {
        Criteria criteria = createEntityCriteria();
        return (List<Tipo>) criteria.list();
    }
    
    /**
    * Funcion que crea una session para conectarse a la BD
    * @return La sesion
    * @author Manuel Leyva    
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    
    /**
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Manuel Leyva    
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Tipo.class);
    }
}