package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Experiencia;
import com.discoverme.www.domain.Oferta;
import com.discoverme.www.repository.OfertaRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Clase que representa un repositorio de ofertas y con ella accedemos a la BD y recuperamso los datos que se necesiten
 * @author Manuel Leyva
 */
@Transactional
@Repository("ofertaRepositoryImpl")
public class OfertaRepositoryImpl implements OfertaRepository{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    /**
    * Funcion devuelve todas las ofertas de la base de datos
    * @return Listado completo de las ofertas de la base de datos
    * @author Jose Luis Sanchez Escoda
    */
    @Override
    public List<Oferta>getAllOfertas() {
        Criteria criteria = createEntityCriteria();
        return (List<Oferta>) criteria.list();
    }
    
    /**
    * Funcion devuelve todas las ofertas de servicios de la base de datos
    * @return Listado completo de las ofertas de servicios de la base de datos
    * @author Manuel Leyva  
    */
    @Override
    public List<Oferta>getOfertasServicios() {
        Criteria criteria = createEntityCriteria();  
        criteria.add(Restrictions.isNull("experiencia"));
        return (List<Oferta>) criteria.list();
    }
    
    /**
    * Funcion devuelve todas las ofertas de ordenadas por exeriencias de la base de datos
     * @param experiencia
    * @return Listado completo de las ofertas de la base de datos
    * @author Manuel Leyva  
    */
    @Override
    public List<Oferta> getOfertasByExperiencia(Experiencia experiencia) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("experiencia", experiencia));
        return (List<Oferta>) criteria.list();
    }
    
    /**
    * Funcion devuelve todas las ofertas por código de la base de datos
     * @param codigo
    * @return Listado completo de las ofertas por código de la base de datos
    * @author Manuel Leyva  
    */    
    @Override
    public Oferta getOfertaByCodigo(String codigo) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("codigo", codigo));
        return (Oferta) criteria.uniqueResult();
    }

    /**
    * Funcion devuelve todas las ofertas por nombre de la base de datos
    * @return Listado completo de las ofertas por nombre de la base de datos
    * @author Manuel Leyva  
    */    
    @Override
    public Oferta getOfertaByNombre(String nombre) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("nombre", nombre));
        return (Oferta) criteria.uniqueResult();
    }
    
    /*
    * Funcion devuelve todas las ofertas por id de la base de datos
    * @return Listado completo de las ofertas por id de la base de datos
    * @author Manuel Leyva  
    */    
    @Override
    public Oferta getOfertaById(Integer id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));
        return (Oferta) criteria.uniqueResult();
    }

    /*
    * Funcion que agrega las ofertas a la base de datos
    * @author Manuel Leyva  
    */    
    @Override
    public void addOferta(Oferta oferta) {
        getSession().saveOrUpdate(oferta);
    }

    /*
    * Funcion que agrega las ofertas a la base de datos
    * @author Manuel Leyva  
    */      
    @Override
    public void deleteOferta(Oferta oferta) {
        getSession().remove(oferta);
    }
    
    /*
    * Funcion que crea una session para conectarse a la BD
    * @return La sesion
    * @author Manuel Leyva    
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    
    /*
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Manuel Leyva    
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Oferta.class);
    }

}
