package com.discoverme.www.repository.impl;

import com.discoverme.www.domain.Experiencia;
import com.discoverme.www.domain.Usuario;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.discoverme.www.repository.ExperienciaRepository;

/**
 * Clase que representa un repositorio de experiencias/actividades con ella accedemos a la BD y recuperamso los datos que se necesiten
 * @author Manuel Leyva
 */
@Transactional
@Repository("experienciaRepositoryImpl")
public class ExperienciaRepositoryImpl implements ExperienciaRepository{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    /**
    * Funcion devuelve todas experiencias de la base de datos
    * @return Listado completo de las experiencias de la base de datos
    * @author Jose Luis Sanchez Escoda
    */
    @Override
    public List<Experiencia>getAllExperiencias() {
        Criteria criteria = createEntityCriteria();    
        return (List<Experiencia>) criteria.list();
    }
    
    /**
    * Funcion que devuelve todos los experiencias/actividades de la BD
     * @param orden
    * @return Listado completo de experiencias/actividades de la BD
    * @author Manuel Leyva    
    */
    @Override
    public List<Experiencia> getAllExperiencias(String orden) {
        Criteria criteria = createEntityCriteria();
        if(orden != null){
            switch (orden){
                case "precio":
                    criteria.addOrder(Order.asc("precio"));
                    break;
                case "puntuacion":
                    criteria.addOrder(Order.desc("puntuacion"));
                    break;
                case "distancia":
                    criteria.addOrder(Order.asc("mins_distancia"));
                    break;
            }
        }
        return (List<Experiencia>) criteria.list();
    }

    /**
    * Funcion que pasandole una id devuelve el experiencia/actividad correspondiente
    * @param id Identificador del experiencia que queremos recuperar
    * @return Experiencia/actividad de la BD
    * @author Manuel Leyva
    */
    @Override
    public Experiencia getExperienciaById(Integer id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("id", id));        
        return (Experiencia) criteria.uniqueResult();
    }
    
    /**
     * Funcion que devuelve una experiencia dado su nombre
     * @param nombre
     * @return
     * @author Manuel Leyva
     */
    @Override
    public Experiencia getExperienciaByNombre(String nombre) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("nombre", nombre));        
        return (Experiencia) criteria.uniqueResult();
    }
    
    /**
    * Funcion que guarda en la BD un experiencia/actividad
    * @param experiencia Experiencia/actividad para guardar en la BD
    * @author Manuel Leyva    
    */
    @Override
    public void addExperiencia(Experiencia experiencia) {
        getSession().saveOrUpdate(experiencia);
    }
    
    /**
     * Funcion que lista experiencias por usuario
     * @param usuario
     * @return 
     * @author Manuel Leyva
     */
    @Override
    public List<Experiencia> getExperienciaByUsuario(Usuario usuario) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("usuario", usuario));        
        return (List<Experiencia>) criteria.list();
    }
    
    /**
     * Funcion que borra experiencias
     * @param experiencia 
     * @author Manuel Leyva
     */
    @Override
    public void deleteExperiencia(Experiencia experiencia) {
        getSession().remove(experiencia);
    }
    
    /**
    * Funcion que crea una session para conectarse a la BD
    * @return La sesion
    * @author Manuel Leyva    
    */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
    
    /**
    * Funcion que crea un objeto Criteria
    * @return El objeto Criteria
    * @author Manuel Leyva    
    */
    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Experiencia.class);
    }

}
