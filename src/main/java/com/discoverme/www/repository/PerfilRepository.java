package com.discoverme.www.repository;

import com.discoverme.www.domain.Perfil;
import java.util.List;

/**
 * Interficie que representa un repositorio de perfiles de huesped
 *
 * @author Manuel Leyva
 */
public interface PerfilRepository {
    Perfil getPerfilById(Integer id);
    Perfil getPerfilByNombre(String nombre);
    List<Perfil> getAllPerfiles();
}
