/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.discoverme.www.repository;

import com.discoverme.www.domain.Foto;
import java.util.List;

/**
 * Interfaz que representa un repositorio de fotos
 * 
 * @author leyva
 */
public interface FotoRepository {
    Foto getFotoById(int id);
    Foto getFotoByNombre(String nombre);
    void addFoto(Foto foto);
    void deleteFoto(Foto foto);
    List<Foto> getAllAvatares();
}
