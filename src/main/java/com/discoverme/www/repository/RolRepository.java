/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.discoverme.www.repository;

import com.discoverme.www.domain.Rol;
import java.util.List;

/**
 * Interficie que representa un repositorio de perfiles de rol
 * @author leyva
 */
public interface RolRepository {
    Rol GetRolById(int id);
    Rol GetRolByNombre(String nombre);
    List<Rol> getAllRoles();
}
