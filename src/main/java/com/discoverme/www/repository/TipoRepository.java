package com.discoverme.www.repository;

import com.discoverme.www.domain.Tipo;
import java.util.List;

/**
 * Interficie que representa un repositorio de tipos/preferencias
 *
 * @author Manuel Leyva
 */
public interface TipoRepository {
    Tipo getTipoById(Integer id);
    List<Tipo> getAllTipos();
}
