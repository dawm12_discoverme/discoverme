/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.discoverme.www.service;

import com.discoverme.www.domain.Foto;
import java.util.List;

/**
 * Interficie que nos da servicio para acceder al repositorio fotos
 * @author leyva
 */
public interface FotoService {
    Foto getFotoById(int id);
    void addFoto(Foto foto);
    void deleteFoto(Foto foto);
    List<Foto> getAllAvatares();
}
