/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.discoverme.www.service.impl;

import com.discoverme.www.domain.Rol;
import com.discoverme.www.repository.RolRepository;
import com.discoverme.www.service.RolService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Clase que nos da servicio para acceder al repositorio de roles
 * @author leyva
 */
@Service
public class RolServiceImpl implements RolService {

    @Autowired
    RolRepository rolRepository;

    /**
     * Funcion que devuelve el tipo de rol dado un nombre
     * @param nombre
     * @return
     * @author Manuel Leyva
     */
    @Override
    public Rol GetRolByNombre(String nombre) {
        return rolRepository.GetRolByNombre(nombre);
    }

    /**
     * Funcion que devuelve un listado con todos los roles
     * @return
     * @author Manuel Leyva
     */
    @Override
    public List<Rol> getAllRoles() {
        return rolRepository.getAllRoles();
    }
}
