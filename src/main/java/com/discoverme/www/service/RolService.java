/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.discoverme.www.service;

import com.discoverme.www.domain.Rol;
import java.util.List;

/**
 * Interficie que nos da servicio para acceder al repositorio de roles
 * @author leyva
 */
public interface RolService {
    Rol GetRolByNombre(String nombre);
    List<Rol> getAllRoles();
}
