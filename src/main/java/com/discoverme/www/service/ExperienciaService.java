package com.discoverme.www.service;

import com.discoverme.www.domain.Experiencia;
import com.discoverme.www.domain.Usuario;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

/**
 * Interficie que nos da servicio para acceder al repositorio de experiencias/actividades
 * @author Manuel Leyva
 */
public interface ExperienciaService {
    List<Experiencia>getAllExperiencias();
    List<Experiencia> getAllExperiencias(String orden);
    Experiencia getExperienciaById(Integer id);
    Experiencia getExperienciaByNombre(String nombre);
    List<Experiencia> getExperienciaByUsuario(Usuario usuario);
    List<Experiencia> getExperienciasByPreferencias(String[] preferencias,String orden);
    void addExperiencia(Usuario usuario,String nombre,String fecha_inicio,String fecha_fin,String hora_inicio,String hora_fin,String descripcion,
                  String precio,String distancia,String mapa,MultipartFile imagenDestacada,MultipartFile imagen1,MultipartFile imagen2,MultipartFile imagen3,String tipo);
    void updateExperiencia(Integer id,Usuario usuario,String nombre,String fecha_inicio,String fecha_fin,String hora_inicio,String hora_fin,String descripcion,
                           String precio,String distancia,String mapa,MultipartFile imagenDestacada,MultipartFile imagen1,MultipartFile imagen2,MultipartFile imagen3,String tipo);
    void deleteExperiencia(Experiencia experiencia);
}
